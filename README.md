# Flectra Community / purchase-reporting

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[purchase_backorder](purchase_backorder/) | 2.0.1.1.0| Report of Un-Invoiced Goods Received and Backorders
[purchase_comment_template](purchase_comment_template/) | 2.0.1.0.0| Comments texts templates on Purchase documents


